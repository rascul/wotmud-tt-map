#!/bin/bash

tmux new-session -s wotmut -d tt++ nalav.tin
tmux set -g status off
tmux set-window-option -g mouse on
tmux split-window -h -l 60 tail -f .wotmud-comms
tmux split-window -v -l 30 tail -f map.out
tmux select-pane -L
tmux attach-session -d
