wotmud-tt-map
=============

This repository containes a [WoTMUD](https://wotmud.info) map for the
[TinTin++](https://tintin.mudhalla.net) mud client. The original map is by Adael
for Mudlet, from [https://github.com/weisluke/WoTMUD](https://github.com/weisluke/WoTMUD).
It is converted to TinTin++ format by using [mapconv](https://gitlab.com/rascul/mapconv/).

![tintin++ with map](https://gitlab.com/rascul/wotmud-tt-map/-/raw/master/shot.png)

Currently, the map and accompanying script is not yet complete and not yet suitable
for regular usage. I recommend using it only for testing.

To use it, you'll need to download the [map](https://gitlab.com/rascul/wotmud-tt-map/-/raw/master/map.map?inline=false)
along with the associated [script](https://gitlab.com/rascul/wotmud-tt-map/-/raw/master/map.tin?inline=false).
The script provides the necessary actions for the map to follow you around on the mud.
Be sure to review the script. It may use aliases that you already use so those
should be reviewed. Put both the script and the map in the directory you start
TinTin++ from, then perform the following:

1. Start in Caemlyn Central Square. To start somewhere else you'll need to know the
room number, which you can find with `#map info` when you're in the room with the map.

2. Load the map:
```
#map read map.map
```

3. Tell the map to go to Caemlyn Central Square (or another room if you have the number):
```
#map goto 97
```

4. Load the script:
```
#read map.tin
```

5. To display the map, you have two options:

  * Split the window and display the map at the top:
    ```
	#split 16 1
	#map flag vtmap
    ```

  * Display the map in another window:

     1. Setup the client:
        ```
        maprows 20
        maprows 60
        mapout /tmp/map.out
        ```

     2. Display the map, open up a new terminal emulator and follow the file with
        `tail -f`. You might use xterm:
        ```
        xterm -geometry 60x20 -rv +sb
        tail -f /tmp/map.out 2>/dev/null
        ```

  * Use tmux script (`tmux.sh`) to load up a tmux session like mine. It may take
    some customizing.

